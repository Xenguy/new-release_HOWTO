## Create file structure on www for chimaera
- Create a "chimaera" directory on new-beta branch at :
 www.devuan.org/source/os/documentation/install-guides/chimaera 

## Add /css and /img directories to the new /chimaera directory then:
- Copy 	debinstall.css to /css from:
https://git.devuan.org/devuan/installer-iso/src/branch/wip/docs/docs/docs/css/debinstall.css
  - (plasma41) Where did this css file come from originally? It contains a comment line attributing it to "Kalle Söderman", but I've never heard of this person. The earliest appearance of this file that I can find is in [this commit](https://git.devuan.org/devuan/installer-iso/commit/14f0503d55c39ff11f721ed37a7e48195232d2b8), but the commit message is no more descriptive than "Upload New File", which conveys no useful information. ...Ok, I just found an even earlier version of the file on the Jessie install media. Still attributed to "Kalle Söderman". I still have no idea who this is. ...Ok, I've finally worked out that this file is derived from the 'debinstall.css' file from the 'data/<codename>' directories in [debian-cd repo](https://salsa.debian.org/images-team/debian-cd). This sort of attribution and provenance __needs__ to be preserved in the form of detailed commit messages. "Upload New File" is just not acceptable. We __need__ to be better than this.
  - NOTE: Please make sure your editor does not do this - https://git.devuan.org/devuan/www.devuan.org/raw/branch/new-beta/source/os/documentation/install-guides/beowulf/css/debinstall.css - to it. I think that happened when rrq moved the site to gitea. That mess needs to be undone asap.

- Copy all the images to /img from:
     https://git.devuan.org/devuan/installer-iso/src/branch/wip/docs/docs/docs/img

## Copy the following files to /chimaera from:
- https://git.devuan.org/devuan/installer-iso/src/branch/wip/docs/docs/docs

  - https://git.devuan.org/devuan/installer-iso/src/branch/wip/docs/docs/docs/full-disk-encryption.html

  - https://git.devuan.org/devuan/installer-iso/src/branch/wip/docs/docs/docs/install-devuan.html

  - https://git.devuan.org/devuan/installer-iso/src/branch/wip/docs/docs/docs/network-configuration.html
  
## Adding docs to www pages

- On https://beta.devuan.org/os/install
  - Upgrade or Migrate to Chimaera
    - Upgrade from Devuan Beowulf (needs to be created?)
    - Automated migration from a Debian bullseye/buster system to Devuan chimaera/beowulf [link to https://git.devuan.org/rrq/buster-to-devuan/src/branch/master]
    - Automatic desktop migration from Debian Bullseye to Devuan Chimaera [link to https://git.devuan.org/farmatito/migration.git] (not quite ready for primetime yet)

  - Install Chimaera (with screenshots)
    - Install Chimaera [link to .../chimaera/install-devuan.html]
    - Full Disk Encryption [link to .../chimaera/full-disk-encryption.html]
    - Live Install [link to https://www.devuan.org/os/documentation/install-guides/beowulf/live-gui ]
    - Network Configuration [link to .../chimaera/network-configuration.html]

  - Additional documentation

    This section will only be used if/when plasma reviews chillfan's and feels they are helpful. Ditto translations.

  - Non-free firmware

    - Non-free firmware packages are available on all Beowulf install media.
  
   could be changed to (or maybe not)
        
    - Non-free firmware packages are available on all Devuan install media.


## Update the "Explore" page
   https://beta.devuan.org/os/explore

### Install

  - Install Devuan (goes to "Install" page)  
    Upgrade or Migrate to Chimaera (add)  
    Install Chimaera (add)  
    Upgrade or Migrate to Beowulf  
    Install Beowulf  
    Install ASCII  
    Additional Documentation (may be removed)  
    Spanish translation (may be removed)  
    Italian translation (may be removed)

### Get Devuan

  - Download Devuan
  - Devuan Derivatives
  - Devuan Derivatives Support
  - Devuan Beowulf Overview  <--- (Change to Chimaera goes to the os index page)
  - Devuan Beowulf Release Notes  <--- (Change to Chimaera goes to files.d.o)

### Install

  - Install Devuan (goes to "Install" page)
  - Upgrade or Migrate to Chimaera (goes to Chimaera section on "Install" page)
  - Install Chimaera (goes to Chimaera section on install page)
  - Install Beowulf (no change. Goes to position on page)
  - Install ASCII (no change. Goes to position on page)
  - Additional Documentation (possibly will be removed)
  - Spanish translation (possibly will be removed)
  - Italian translation (possibly will be removed)

### Development
  - https://jenkins.devuan.dev
  - Community ARM Development 
  - Community (Add mention of docker images as per the below.  Also need to be added to https://beta.devuan.org/get-devuan after the arm entry)
     - (2021-09-09) docker images daily updated for ascii, beowulf, chimaera and ceres 
    https://hub.docker.com/r/dyne/devuan/tags?page=1&ordering=last_updated
    by paddy-hack and jaromil, readmes:
  - https://github.com/dyne/docker-devuan-builds
  - https://gitlab.com/paddy-hack/devuan/
    
  - Devuan’s Continuous Integraton System (change url to https://jenkins.devuan.dev)

